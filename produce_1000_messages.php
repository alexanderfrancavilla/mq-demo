<?php
require __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

// Connect to the rabbit
$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

// Declare a queue, in case it doesn't exist yet
$channel->queue_declare('some_queue', false, false, false, false);

// Create 1000 messages
for ($i=0; $i < 1000; $i++) {
    $channel->basic_publish(
        new AMQPMessage('index_'.$i),   // the actual message
        array('delivery_mode' => 2),    // delivery_mode 2 means "persistent"
        'some_queue'                    // queue name
    );
}

$channel->close();
$connection->close();
