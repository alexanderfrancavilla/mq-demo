# mq-demo
Basic MessageQueue demo using a containered RabbitMQ and producers/consumers in php using php-amqplib.

## Requirements
* PHP (with `bcmath`-extension)
* Docker (>1.10) + Docker-Compose

## Usage
Prepare Composer and bring RabbitMQ up:

* Pull in Dependencies via `php composer.phar install`
* Start RabbitMQ (as a docker container): `docker-compose up -d`
* RabbitMQ Management plugin should be available atfer some time at: [http://localhost:15672](http://localhost:15672) (User/PW is guest)

Then fill the queue and play around with workers.

## Example (Linux)
Fill the queue with 1,000 messages:
```
php produce_1000_messages.php
```
Open up the [Management Console](http://localhost:15672), and spawn some workers:
```
php worker.php &
php worker.php &
php worker.php &
```
