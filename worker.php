<?php
require __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

// Connect to the rabbit
$connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
$channel = $connection->channel();

// Declare a queue, in case it doesn't exist yet
$channel->queue_declare('some_queue', false, false, false, false);

// This is "process"-function, which gets executed per message
$process = function($msg) {
    // $msg->body contains the actual message / task

    // Simulate some work
    usleep(500000);

    // Acknowledge, that this message has been processed
    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
};

$channel->basic_qos(null, 1, null);
$channel->basic_consume('some_queue', '', false, false, false, false, $process);

// Process messages for ever
while(count($channel->callbacks)) {
    $channel->wait();
}
